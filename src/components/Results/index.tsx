import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';

export default function Results() {
  return (
    <div style={{ height: 400, width: '100%' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <div style={{ flexGrow: 1 }}>
        <DataGrid
          columns={[{ field: 'username' }, { field: 'age' }]}
          rows={[
            {
              id: 1,
              username: '@MUI',
              age: 20,
            },
          ]}
        />
        </div>
      </div>
    </div>
  );
}
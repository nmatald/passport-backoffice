import Button from '@mui/material/Button';

export const ButtonSimple = ({ text }: { text: string}) => {
  return <Button variant="contained">{text}</Button>
}